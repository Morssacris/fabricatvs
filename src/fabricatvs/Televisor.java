/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabricatvs;

/**
 *
 * @author William
 */
public class Televisor {

    /**
     *
     */
    private String marca;
    protected boolean estado = false;
    protected byte volumen = 0;
    protected byte volumenp = volumen;
    private double largo;
    private double alto;
    private Puerto[] puertos;
    protected Puerto entrada;
    protected Canal canal;
    protected boolean mute;

    //metodos
    public void encender() {
        estado = estado ? !estado : !estado;
    }

    public void subirVol() {
        if (estado == true) {
            if (mute == false) {
                if (volumen < 100) {
                    volumen++;
                    System.out.println("El volumen es" + volumen);
                }
            }
            if (mute == true) {
                mute = false;
                volumen++;
                System.out.println("Volumen es: " + volumen);
            }
        }

    }

    public void bajarVol() {
        if (estado == true && volumen > 0) {
            volumen--;
            System.out.println("El voluemn es " + volumen);
        } else {
            System.out.println("Encienda el TV primero");
        }
    }

    public int calcularPulgadas(double alto, double largo) {
        double a = Math.pow(alto, 2.0);
        System.out.println(a);
        double b = Math.pow(largo, 2.0);
        System.out.println(b);
        double hi = a + b;
        System.out.println(hi);
        hi = Math.pow(hi, 0.5);
        System.out.println(hi);

        return (int) (hi);
    }

    public Televisor(String marca, double largo, double alto) {
        this.marca = marca;
        this.largo = largo;
        this.alto = alto;
        /**
         * @param marca Indica la marca del televisor
         * @param largo Indica el largo del televisor
         * @param alto Indica el alto del televisor Este metodo es el
         * constructor para un televisor simple
         * @since La clase del dia 14 de junio de 2016
         */
    }

    public void mute() {
        if (estado == true) {
            if (mute == false) {
                mute = true;
                volumenp = 0;
                System.out.println("Mute: " + mute);
                System.out.println("Volumen: " + volumenp);
            } else {
                mute = false;
                System.out.println("Mute: " + mute);
                System.out.println("Volumen: " + volumen);
            }
        }

    }

}
