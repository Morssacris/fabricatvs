/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabricatvs;

/**
 *
 * @author William
 */
public class Canal 
{
    private byte [] numero;
    private String [] nombre;
    private String [] clasificacion;

    /**
     * @return the numero
     */
    public byte[] getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(byte[] numero) {
        this.numero = numero;
    }

    /**
     * @return the nombre
     */
    public String[] getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String[] nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the clasificacion
     */
    public String[] getClasificacion() {
        return clasificacion;
    }

    /**
     * @param clasificacion the clasificacion to set
     */
    public void setClasificacion(String[] clasificacion) {
        this.clasificacion = clasificacion;
    }
    
}
